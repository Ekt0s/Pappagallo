package io.louis_gabriel.pappagallo.ui.injection.module

import android.app.Application
import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import io.louis_gabriel.pappagallo.cache.PreferencesHelper
import io.louis_gabriel.pappagallo.data.executor.JobExecutor
import io.louis_gabriel.pappagallo.data.repository.BufferooCache
import io.louis_gabriel.pappagallo.data.repository.BufferooRemote
import io.louis_gabriel.pappagallo.domain.executor.PostExecutionThread
import io.louis_gabriel.pappagallo.domain.executor.ThreadExecutor
import io.louis_gabriel.pappagallo.domain.repository.BufferooRepository
import io.louis_gabriel.pappagallo.remote.BufferooService
import io.louis_gabriel.pappagallo.ui.UiThread
import io.louis_gabriel.pappagallo.ui.injection.scopes.PerApplication

@Module
class TestApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    internal fun providePreferencesHelper(): PreferencesHelper {
        return mock()
    }

    @Provides
    @PerApplication
    internal fun provideBufferooRepository(): BufferooRepository {
        return mock()
    }

    @Provides
    @PerApplication
    internal fun provideBufferooCache(): BufferooCache {
        return mock()
    }

    @Provides
    @PerApplication
    internal fun provideBufferooRemote(): BufferooRemote {
        return mock()
    }

    @Provides
    @PerApplication
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @PerApplication
    internal fun provideBufferooService(): BufferooService {
        return mock()
    }

}