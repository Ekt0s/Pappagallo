package io.louis_gabriel.pappagallo.ui.injection.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import io.louis_gabriel.pappagallo.domain.executor.PostExecutionThread
import io.louis_gabriel.pappagallo.domain.repository.BufferooRepository
import io.louis_gabriel.pappagallo.ui.injection.ApplicationComponent
import io.louis_gabriel.pappagallo.ui.injection.module.ActivityBindingModule
import io.louis_gabriel.pappagallo.ui.injection.module.TestApplicationModule
import io.louis_gabriel.pappagallo.ui.injection.scopes.PerApplication
import io.louis_gabriel.pappagallo.ui.test.TestApplication

@Component(modules = arrayOf(TestApplicationModule::class, ActivityBindingModule::class,
        AndroidSupportInjectionModule::class))
@PerApplication
interface TestApplicationComponent : ApplicationComponent {

    fun bufferooRepository(): BufferooRepository

    fun postExecutionThread(): PostExecutionThread

    fun inject(application: TestApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): TestApplicationComponent.Builder

        fun build(): TestApplicationComponent
    }

}