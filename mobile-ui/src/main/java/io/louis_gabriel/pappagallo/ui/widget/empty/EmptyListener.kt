package io.louis_gabriel.pappagallo.ui.widget.empty

interface EmptyListener {

    fun onCheckAgainClicked()

}