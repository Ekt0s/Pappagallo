package io.louis_gabriel.pappagallo.ui.injection.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.louis_gabriel.pappagallo.ui.browse.BrowseActivity
import io.louis_gabriel.pappagallo.ui.injection.scopes.PerActivity

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseActivityModule::class))
    abstract fun bindMainActivity(): BrowseActivity

}