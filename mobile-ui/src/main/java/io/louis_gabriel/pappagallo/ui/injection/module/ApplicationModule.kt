package io.louis_gabriel.pappagallo.ui.injection.module

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import io.louis_gabriel.pappagallo.cache.BufferooCacheImpl
import io.louis_gabriel.pappagallo.cache.PreferencesHelper
import io.louis_gabriel.pappagallo.cache.db.BufferoosDatabase
import io.louis_gabriel.pappagallo.cache.mapper.BufferooEntityMapper
import io.louis_gabriel.pappagallo.data.BufferooDataRepository
import io.louis_gabriel.pappagallo.data.executor.JobExecutor
import io.louis_gabriel.pappagallo.data.mapper.BufferooMapper
import io.louis_gabriel.pappagallo.data.repository.BufferooCache
import io.louis_gabriel.pappagallo.data.repository.BufferooRemote
import io.louis_gabriel.pappagallo.data.source.BufferooDataStoreFactory
import io.louis_gabriel.pappagallo.domain.executor.PostExecutionThread
import io.louis_gabriel.pappagallo.domain.executor.ThreadExecutor
import io.louis_gabriel.pappagallo.domain.repository.BufferooRepository
import io.louis_gabriel.pappagallo.remote.BufferooRemoteImpl
import io.louis_gabriel.pappagallo.remote.BufferooService
import io.louis_gabriel.pappagallo.remote.BufferooServiceFactory
import io.louis_gabriel.pappagallo.ui.BuildConfig
import io.louis_gabriel.pappagallo.ui.UiThread
import io.louis_gabriel.pappagallo.ui.injection.scopes.PerApplication

/**
 * Module used to provide dependencies at an application-level.
 */
@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    internal fun providePreferencesHelper(context: Context): PreferencesHelper {
        return PreferencesHelper(context)
    }


    @Provides
    @PerApplication
    internal fun provideBufferooRepository(factory: BufferooDataStoreFactory,
                                           mapper: BufferooMapper): BufferooRepository {
        return BufferooDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideBufferooCache(database: BufferoosDatabase,
                                      entityMapper: BufferooEntityMapper,
                                      helper: PreferencesHelper): BufferooCache {
        return BufferooCacheImpl(database, entityMapper, helper)
    }

    @Provides
    @PerApplication
    internal fun provideBufferooRemote(service: BufferooService,
                                       factory: io.louis_gabriel.pappagallo.remote.mapper.BufferooEntityMapper): BufferooRemote {
        return BufferooRemoteImpl(service, factory)
    }

    @Provides
    @PerApplication
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @PerApplication
    internal fun provideBufferooService(): BufferooService {
        return BufferooServiceFactory.makeBuffeoorService(BuildConfig.DEBUG)
    }

    @Provides
    @PerApplication
    internal fun provideViewModelFactory(): ViewModelProvider.Factory {
        return ViewModelProvider.NewInstanceFactory()
    }

    @Provides
    @PerApplication
    internal fun provideBufferoosDatabase(application: Application): BufferoosDatabase {
        return Room.databaseBuilder(application.applicationContext,
                BufferoosDatabase::class.java, "bufferoos.db")
                .build()
    }

}
