package io.louis_gabriel.pappagallo.ui.injection.module

import dagger.Module
import dagger.Provides
import io.louis_gabriel.pappagallo.domain.interactor.browse.GetBufferoos
import io.louis_gabriel.pappagallo.presentation.browse.BrowseBufferoosViewModelFactory
import io.louis_gabriel.pappagallo.presentation.mapper.BufferooMapper


/**
 * Module used to provide dependencies at an activity-level.
 */
@Module
open class BrowseActivityModule {

    @Provides
    fun provideBrowseBufferoosViewModelFactory(getBufferoos: GetBufferoos,
                                               bufferooMapper: BufferooMapper):
            BrowseBufferoosViewModelFactory {
        return BrowseBufferoosViewModelFactory(getBufferoos, bufferooMapper)
    }

}
