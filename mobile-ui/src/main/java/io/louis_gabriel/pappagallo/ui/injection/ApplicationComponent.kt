package io.louis_gabriel.pappagallo.ui.injection

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import io.louis_gabriel.pappagallo.ui.BufferooApplication
import io.louis_gabriel.pappagallo.ui.injection.module.ActivityBindingModule
import io.louis_gabriel.pappagallo.ui.injection.module.ApplicationModule
import io.louis_gabriel.pappagallo.ui.injection.scopes.PerApplication

@PerApplication
@Component(modules = arrayOf(ActivityBindingModule::class, ApplicationModule::class,
        AndroidSupportInjectionModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

    fun inject(app: BufferooApplication)

}
