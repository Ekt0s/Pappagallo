package io.louis_gabriel.pappagallo.ui.model

/**
 * Representation for a [BufferooViewModel] fetched from an external layer data source
 */
class BufferooViewModel(val name: String, val title: String, val avatar: String)