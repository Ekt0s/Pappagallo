package io.louis_gabriel.pappagallo.ui.widget.error

interface ErrorListener {

    fun onTryAgainClicked()

}