package io.louis_gabriel.pappagallo.presentation.test.factory

import io.louis_gabriel.pappagallo.domain.model.Bufferoo
import io.louis_gabriel.pappagallo.presentation.model.BufferooView
import io.louis_gabriel.pappagallo.presentation.test.factory.DataFactory.Factory.randomLong
import io.louis_gabriel.pappagallo.presentation.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for Bufferoo related instances
 */
class BufferooFactory {

    companion object Factory {

        fun makeBufferooList(count: Int): List<Bufferoo> {
            val bufferoos = mutableListOf<Bufferoo>()
            repeat(count) {
                bufferoos.add(makeBufferooModel())
            }
            return bufferoos
        }

        fun makeBufferooModel(): Bufferoo {
            return Bufferoo(randomLong(), randomUuid(), randomUuid(), randomUuid())
        }

        fun makeBufferooViewList(count: Int): List<BufferooView> {
            val bufferoos = mutableListOf<BufferooView>()
            repeat(count) {
                bufferoos.add(makeBufferooView())
            }
            return bufferoos
        }

        fun makeBufferooView(): BufferooView {
            return BufferooView(randomUuid(), randomUuid(), randomUuid())
        }

    }

}