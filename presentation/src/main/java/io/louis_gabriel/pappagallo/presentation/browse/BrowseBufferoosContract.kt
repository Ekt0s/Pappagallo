package io.louis_gabriel.pappagallo.presentation.browse

import io.louis_gabriel.pappagallo.presentation.BasePresenter
import io.louis_gabriel.pappagallo.presentation.BaseView
import io.louis_gabriel.pappagallo.presentation.model.BufferooView

/**
 * Defines a contract of operations between the Browse Presenter and Browse View
 */
interface BrowseBufferoosContract {

    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun showBufferoos(bufferoos: List<BufferooView>)

        fun hideBufferoos()

        fun showErrorState()

        fun hideErrorState()

        fun showEmptyState()

        fun hideEmptyState()

    }

    interface Presenter : BasePresenter {

        fun retrieveBufferoos()

    }

}