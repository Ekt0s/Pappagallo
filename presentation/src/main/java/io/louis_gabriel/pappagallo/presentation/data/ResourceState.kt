package io.louis_gabriel.pappagallo.presentation.data

/**
 * Represents the state in which a [Resource] is currently in
 */
enum class ResourceState {
    LOADING, SUCCESS, ERROR
}