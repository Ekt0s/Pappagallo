package io.louis_gabriel.pappagallo.domain.interactor.browse

import io.reactivex.Flowable
import io.louis_gabriel.pappagallo.domain.executor.PostExecutionThread
import io.louis_gabriel.pappagallo.domain.executor.ThreadExecutor
import io.louis_gabriel.pappagallo.domain.interactor.FlowableUseCase
import io.louis_gabriel.pappagallo.domain.model.Bufferoo
import io.louis_gabriel.pappagallo.domain.repository.BufferooRepository
import javax.inject.Inject

/**
 * Use case used for retreiving a [List] of [Bufferoo] instances from the [BufferooRepository]
 */
open class GetBufferoos @Inject constructor(val bufferooRepository: BufferooRepository,
                                            threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread):
        FlowableUseCase<List<Bufferoo>, Void?>(threadExecutor, postExecutionThread) {

    public override fun buildUseCaseObservable(params: Void?): Flowable<List<Bufferoo>> {
        return bufferooRepository.getBufferoos()
    }

}