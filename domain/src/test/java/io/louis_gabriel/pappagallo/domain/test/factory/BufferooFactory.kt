package io.louis_gabriel.pappagallo.domain.test.factory

import io.louis_gabriel.pappagallo.domain.model.Bufferoo
import io.louis_gabriel.pappagallo.domain.test.factory.DataFactory.Factory.randomLong
import io.louis_gabriel.pappagallo.domain.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for Bufferoo related instances
 */
class BufferooFactory {

    companion object Factory {

        fun makeBufferooList(count: Int): List<Bufferoo> {
            val bufferoos = mutableListOf<Bufferoo>()
            repeat(count) {
                bufferoos.add(makeBufferoo())
            }
            return bufferoos
        }

        fun makeBufferoo(): Bufferoo {
            return Bufferoo(randomLong(), randomUuid(), randomUuid(), randomUuid())
        }

    }

}